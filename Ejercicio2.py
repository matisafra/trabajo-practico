class ArbolBinario (object):
    
   """ implementación de ArbolBinario con raíz, rama derecha e izquierda"""
   
   def __init__(self): #devuelve un arbol vacio.
       self._a = None #raiz
       self._izq= None #sub-arbol izquierdo
       self._der= None #sub-arbol derecho
        
   def vacio (self): #indica si el arbol se encuentra vacio
       if self._a == None and self._izq == None and self._der == None:
           return True
       else:
           return False
       
   def raiz (self): #si el arbol se encuentra vacío devuelve None por default, si no devuelve un entero
           return self._a
       
   def bin (self, a, izq, der): #modifica el parametro implicito por a, izq y der. Donde izq y der son sub-arboles.
       self._a = a
       self._izq = izq
       self._der = der
             
   def izquierda (self): #devuelve el sub-arbol izquierdo
           return self._izq
       
   def derecha (self): #devuelve el sub-arbol derecho
            return self._der

   def find (self, a): #indica si a se encuentra en el arbol
       if self.vacio():
           return False
       else:
           return a == self.raiz() or self.izquierda().find(a) or self.derecha().find(a)
       #si a es igual a la raiz del arbol principal o de alguno de los sub-arboles devuelve True, si no, devuelve False.
       
   def espejo(self): 
        ArbolEspejo = ArbolBinario() #nuevo objeto
        if not self.vacio():
            ArbolEspejo.bin(self.raiz(), self.derecha().espejo(), self.izquierda().espejo())
            #intercambia los parametros recibidos como izq y der en la función bin 
        return ArbolEspejo 
            
   def preorder (self):
        preOrder = [] #lista vacia
        preOrder.append(int(self.raiz())) #appendea la raiz del arbol a la lista como un entero
        if not self.izquierda().vacio(): #aplica la funcion preorder a los sub-arboles izquierdo y derecho
            preOrder.append(self.izquierda().preorder()) 
        if not self.derecha().vacio():
            preOrder.append(self.derecha().preorder())
        return preOrder
    
   def posorder (self):
        posOrder = [] #lista vacia
        if not self.derecha().vacio(): #aplica la funcion posorder a los sub-arboles derecho e izquierdo
            posOrder.append(self.derecha().posorder())
        if not self.izquierda().vacio():
            posOrder.append(self.izquierda().posorder())
        posOrder.append(int(self.raiz())) #appendea la raiz del arbol a la lista como un entero
        return posOrder
    
   def inorder(self):
        inOrder = [] #lista vacia
        if not self.izquierda().vacio(): #aplica la funcion inorder al sub-arbol izquierdo
            inOrder.append(self.izquierda().inorder())
        inOrder.append(int(self.raiz())) #appendea la raiz del arbol a la lista como un entero
        if not self.derecha().vacio(): #aplica la funcion inorder al sub-arbol derecho
            inOrder.append(self.derecha().inorder())
        return inOrder
    
#Ejemplos para probar la clase ArbolBinario
A = ArbolBinario() 
A.bin(3, ArbolBinario(), ArbolBinario()) #sub-arbol izquierdo
B = ArbolBinario()
B.bin(5, ArbolBinario(), ArbolBinario()) #sub-arbol derecho
arbolBinario1 = ArbolBinario()
arbolBinario1.bin(8, A, B) #arbol con raiz 8 y A y B como sub-arboles izquierdo y derecho

print (arbolBinario1.find(8))
print (arbolBinario1.find(3))
print(arbolBinario1.find(5))
print(arbolBinario1.find(7))
print (arbolBinario1.preorder())
print(arbolBinario1.espejo().preorder())
print(arbolBinario1.posorder())
print(arbolBinario1.inorder())

C = ArbolBinario()
C.bin(9, ArbolBinario(), ArbolBinario())
D = ArbolBinario()
D.bin(2, ArbolBinario(), ArbolBinario())
E = ArbolBinario()
E.bin(20, ArbolBinario(), ArbolBinario())
F = ArbolBinario()
F.bin(7, ArbolBinario(), ArbolBinario())
G = ArbolBinario() 
G.bin(15, C, D)
H = ArbolBinario()
H.bin(32, E, F)
arbolBinario2 = ArbolBinario()
arbolBinario2.bin(6, G, H) #arbol con raiz 6 y G y H como sub-arboles izquierdo y derecho

print (arbolBinario2.find(6))
print (arbolBinario2.find(9))
print(arbolBinario2.find(7))
print(arbolBinario2.find(25))
print (arbolBinario2.preorder())
print(arbolBinario2.espejo().preorder())
print(arbolBinario2.posorder())
print(arbolBinario2.inorder())

