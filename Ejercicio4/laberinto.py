class Laberinto(object):
    def __init__(self, parent=None):
        self.parent = parent
        self._laberinto = [] #matriz que representa al laberinto como una lista de listas       
        self._size = (0,0) #cantidad de filas y columnas del laberinto
        self._rata = (0,0) #posicion de la rata
        self._queso = (0,0) #posicion del queso
        self._Info = [] #matriz con la informacion de las celdas visitadas y en el camino actual
        self.camino = [] #Lista que marca el camino recorrido

    def cargar (self, fn):
        datos_laberinto = open (fn,'r') 
        listaLaberinto= datos_laberinto.readlines()
        self._size = listaLaberinto[0].split("Dim(")[1].split(",")
        self._size = (int(self._size[0]),int(self._size[1].split(")")[0]))
        tamano = len(listaLaberinto[1])
        lineas = len(listaLaberinto)
        self._laberinto =[]
        self._Info = []
        for i in range (1, lineas):
            self._laberinto.append(listaLaberinto[i][1:(tamano-2)].split("]["))
        for i in range(len(self._laberinto)):
            for j in range(len(self._laberinto[i])):
                self._laberinto[i][j] = self._laberinto[i][j].split(',') 
                for k in range (len(self._laberinto[i][j])):
                    self._laberinto[i][j][k]=int(self._laberinto[i][j][k])
                    
        self.resetear()
        datos_laberinto.close()
    # cargar abre el archivo de texto que describe laberinto, lo lee, lo procesa y devuelve una lista de listas respetando el formato del apéndice C.
    # al finalizar la carga se resetea el laberinto de acuerdo a la funcion resetear.
        
    def tamano(self):
        return self._size
    #devuelve la dimension indicada en la primer linea del archivo de texto que describe el laberinto.
        
    def resetear(self):
        self._Info = []
        self.camino = []
        for i in range(len(self._laberinto)):
            listaAux = []
            for j in range (len(self._laberinto[i])):
                listaAux.append([False, False])  
            self._Info.append(listaAux)
    #devuelve la matriz con la informacion de las celdas visitadas y el camino actual reseteada
    #para todas las celdas {'visitada':False, 'caminoActual':False}        
        self._rata = (0,0)
        self._queso = (self._size[0]-1, self._size[1]-1)
    #ubica a la rata es la esquina superior izquierda
    #ubica al queso en la esquina inferior derecha
        
    def getPosicionRata(self):
        return self._rata

    def getPosicionQueso(self):
        return self._queso

    def setPosicionRata(self, i, j):
        if 0 <= i < self._size[0] and 0 <= j < self._size[1]:
            self._rata = (i, j)
        return self._rata == (i, j)
    #modifica la posicion de la rata si los valores i y j se encuentran dentro de las dimensiones del laberinto

    def setPosicionQueso(self, i, j):
        if 0 <= i < self._size[0] and 0 <= j < self._size[1]:
            self._queso = (i, j)
        return self._queso == (i, j)
    #modifica la posicion del queso si los valores i y j se encuentran dentro de las dimensiones del laberinto

    def esPosicionRata(self, i, j):
        return self._rata == (i, j)
    
    def esPosicionQueso(self, i, j):
        return self._queso == (i, j)
    
    def get(self, i, j):
        bool_matrix = []
        for k in range(len(self._laberinto[i][j])):
            bool_matrix.append(self._laberinto[i][j][k]==1)
        return bool_matrix
    #devuelve una lista con 4 elementos booleanos que indican si hay pared o no en cada uno de los 4 bordes de la celda (i,j)
    #Si hay pared devuelve True, si no hay pared devuelve False
    
    def getInfoCelda(self, i, j): 
        return {'visitada':self._Info[i][j][0], 'caminoActual': self._Info[i][j][1]}
    
    def resuelto(self):
        return self._rata == self._queso
    #el laberinto se encuentra resuelto si la posicion de la rata coincide con la del queso

    def avanzar(self, i, j):

        if self.resuelto():
            return True
        
        if i < self._size[0] and j < self._size[1] and i >= 0 and j >= 0:
            
            if self.setPosicionRata(i,j-1) and self.get(i, j)[0]==False and self._Info[i][j-1][0]==False: #avanzar hacia la  izquierda
                self._Info[i][j][0]=True #modifica visitado
                self._Info[i][j-1][1]=True #modifica camino actual
                self.camino.append([i,j]) #guarda la ultima posicion visitada
                self._redibujar()
                return self.avanzar(i,j-1)
            
            if self.setPosicionRata(i-1,j) and self.get(i,j)[1]==False and self._Info[i-1][j][0]==False: #avanzar hacia arriba
                self._Info[i][j][0]=True
                self._Info[i-1][j][1]=True
                self.camino.append([i,j])
                self._redibujar()
                return self.avanzar(i-1,j)
            
            if self.setPosicionRata(i,j+1) and self.get(i,j)[2]==False and self._Info[i][j+1][0]==False: #avanzar hacia la derecha
                self._Info[i][j][0]=True
                self._Info[i][j+1][1]=True
                self.camino.append([i,j])
                self._redibujar()
                return self.avanzar(i,j+1)
        
            if self.setPosicionRata(i+1,j) and self.get(i,j)[3]==False and self._Info[i+1][j][0]==False: #avanzar hacia abajo
                self._Info[i][j][0]=True
                self._Info[i+1][j][1]=True
                self.camino.append([i,j])
                self._redibujar()
                return self.avanzar(i+1,j)
        
        return self.retroceder(i,j)
        
    def retroceder(self, i, j): #devuelve a la rata a la posicion anterior
        if len(self.camino) == 0: #si retrocede hasta la posicion inicial no puede resolver el laberinto
            return False
        
        else:
            self.setPosicionRata(self.camino[len(self.camino)-1][0], self.camino[len(self.camino)-1][1]) #devuelve la rata a la última posición appendeada en la lista self.camino
            self.camino.pop() #elimina la ultima posicion appendeada en la lista self.camino
            self._Info[i][j][0]=True #modifica visitada
            self._Info[i][j][1]=False #modifica camino actual
            self._redibujar()
            return self.avanzar(self._rata[0], self._rata[1]) 
        
    def resolver(self): 
        self._Info[self._rata[0]][self._rata[1]][1]=True #marca la posicion de la rata como camino actual
        return self.avanzar(self._rata[0], self._rata[1]) #resuelve el laberinto desde la posicion de la rata
            
    def _redibujar(self):
	    if self.parent is not None:
	        self.parent.update()

lab = Laberinto()

