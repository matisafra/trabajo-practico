#include "filters.h"
#include <stdio.h>
#include <omp.h>

void blur_filter(float * im_res, float * im, int ii, int jj)
{
    #pragma omp parallel
    #pragma omp for
    for(int f=1 ; f<ii-1; f++){
        for(int c=1; c<jj-1; c++){
                im_res[f*jj+c] = (im[((f-1)*jj)+c] + im[((f+1)*jj)+c] + im[f*jj + (c-1)] + im[f*jj + (c + 1)]) / 4;

                }
        }
    }

