#include "filters.h"
#include <stdio.h>
#include <omp.h>

void gray_filter(float * im_res, pixel_t * im, int ii, int jj)
{
    #pragma omp parallel
    #pragma omp for
    for(int f=0 ; f<ii; f++){
        for(int c=0; c<jj; c++){
         im_res[f*jj+c] = im[f*jj+c].r*0.3 + im[f*jj+c].g*0.6 + im[f*jj+c].b*0.11;
        }
    }
}
