from scipy import misc
import numpy as np
import imfilters
import time

#Imagenes que vamos a usar, comentado se muestran sus dimensiones
lemur = misc.imread('lemur.jpg') # 924x530
zorro = misc.imread('zorro.jpg') #590x443
gato = misc.imread('gato.jpg') #470x313


# gray_filter toma array proveniente de una imagen color y devuelve un array con igual cantidad de filas y columnas pero una sola componente de color 
def gray_filter(a): 
    gris = []
    for n in range(len(a)):
        fila = []
        for i in range(len(a[n])):
            res = a[n][i][0]*0.3 + a[n][i][1]*0.6 + a[n][i][2]*0.11
            fila.append(res)
        gris.append(fila)
    return gris

#blur_filter toma el array arrojado por gray_filter y devuelve un array correspondiente a una imagen con blur
def blur_filter(a): 
    blur = []
    for n in range(len(a)):
        fila = []
        for i in range(len(a[n])): #como es en escala de grises, aca accedemos a una componente, no a tres
            if i == 0 or i == len(a[n]) - 1 or n == 0 or n == len(a) - 1:
                res = 0
            else:
                res = (a[n - 1][i] + a[n + 1][i] + a[n][i - 1] + a[n][i + 1]) / 4
            fila.append(res)
        blur.append(fila)
    return blur

#Implementacion de filtros y calculo de tiempos de ejecucion
def implementacion(a): 
#implementacion en Python    
    inicio = time.time()
    bynPython  = gray_filter(a)
    blurPython = blur_filter(bynPython)
    misc.imsave('ImagenGrisPython.jpg',bynPython)
    misc.imsave('ImagenBlurPython.jpg',blurPython)
    final = time.time()
    tiempoPython = float(final-inicio)
#implementacion en c++
    inicio = time.time()
    bynC = imfilters.gray_filter(a)   
    blurC = imfilters.blur_filter(bynC)
    misc.imsave('ImagenGrisC.jpg',bynC)
    misc.imsave('ImagenBlurC.jpg',blurC)
    final = time.time()
    tiempoC = float(final-inicio)
    return ('tiempo Python', tiempoPython, 'tiempo C++', tiempoC)
 
    
print(implementacion(lemur)) #Aca se debe ingresar lemur, zorro o gato
#Cada vez que se ejecute el programa, se obtendran tiempos de ejecucion y se guardaran cuatro imagenes:
#ImagenGrisPython, ImagenBlurPython, ImagenGrisC, ImagenBlurC
#Por este motivo, si se quieren guardar las imagenes y que no se vayan reemplazando, es importante ir cambiandole el nombre  



      
            
            
           