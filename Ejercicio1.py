import time
import random
import sys

def listaDePuntos(fn):# recibe un archivo de texto y genera las tuplas
    tuplas = []
    for fila in fn:
        fila = fila.split(' ')
        tuplas.append((float(fila[0]),float(fila[1])))
    return tuplas
# la funcion listaDePuntos no es utilizada en este programa ya que las tuplas seran generadas aca mismo
# de todas formas, si se quisiera recibir un archivo con las tuplas, listaDePuntos es la funcion para procesarla 

def distancia(p0,p1): #Definimos distancia euclidea utilizada en distanciaMinima
    return (((p0[0] - p1[0])**2) + ((p0[1] - p1[1])**2))**(1/2)

def distanciaMinima(a): #recibe listas de tuplas y devuelve una tupla con el par y la distancia minima por fuerza bruta
    if len(a) <= 1:
        return False  #Cuando hay un elemento o menos, no existe distancia, nosotros lo expresamos como False
    if len(a) == 2: #si len(a) es 2, la distancia minima es aquella entre los dos unicos puntos
        return ((a[0],a[1]),distancia(a[0],a[1]))
    else:
        ref = distancia(a[0],a[1]) #Guarda la distancia minima
        parMinimo = (a[0],a[1]) #Guarda el par de tuplas de la distancia minima
        for i in range(len(a)-1): #Fuerza bruta para calcular distancia minima
            for j in range(i+1,len(a)):
                if distancia(a[i],a[j]) <= ref:
                    ref = distancia(a[i],a[j])
                    parMinimo = (a[i],a[j])
    return (parMinimo,ref) 

#A continuación se definenen los algoritmos de ordenamiento: up, megre y Pyhton

def maxPos(l,li,ls): #l:lista; li:pos lim. inferior; ls:pos lim. superior. Parametros recibidos de up
    max = l[0]
    iMax = 0 #guardo la pos. que tiene el máximo en el rago (n:m)
    for i in range(li,ls):
        if max < l[i]:
            max = l[i]
            iMax = i
    return iMax

def up(a): #recibe una lista y la ordena segun upSort
    i = len(a)-1
    m = 0 #guarda la máxima posición
    while i>0:
        m = maxPos(a,0,i) #definimos los parametros que va a tomar maxPos para que se recorra toda la lista
        if a[m]> a[i]:
            a[m],a[i] = a[i], a[m]
        i -= 1
    return a


def merge(a): #recibe una lista y la ordena segun mergeSort   
    
    if len(a)>1: #cuando las listas son de un elemnto, no entra aca. Luego de la recursion, terminan quedando todas listas de un elemento
        medio = len(a)//2 #Definimos el indice medio a partir de la division entera de la lista por dos
        izq = a[:medio]
        dcha = a[medio:]

        merge(izq) #recursividad para ir dividiendo las listas hasta obtener listas de un elemento 
        merge(dcha) 

        i=0 #indice implementado para la mitad izq
        j=0 #indice implementado para la mitad dcha
        k=0 #indice que va recorriendo toda la lista a
        while i < len(izq) and j < len(dcha):
            if izq[i] < dcha[j]:
                a[k]=izq[i] 
                i+=1
            else:
                a[k]=dcha[j] 
                j+=1
            k+=1

        while i < len(izq):
            a[k]=izq[i]
            i+=1
            k+=1

        while j < len(dcha):
            a[k]=dcha[j]
            j+=1
            k+=1
        return a #lista ordenada segun mergeSort

def python(a): #recibe una lista y la ordena segun el sort de Python
    a.sort()
    return a         

# A partir de aca se calculan las distancias minimas por DyC
    
def distanciaMinimaDyC(a,algoritmo): #Recibe a:lista de tuplas; algoritmo: ordenamiento
    algoritmo(a) #ordena las tuplas segun el algoritmo elegido.Python por default toma la coordenada X para ordenar
    d = divideDyC2(a) #genera lista de lista de tuplas de hasta tres elementos y combina los resultados
    return d
 
def divideDyC2(a): 
    if len(a)<=3: #caso base
        return distanciaMinima(a)
    else:
        medio = len(a)//2 #Dividimos los puntos en mitades cortando sobre el eje x
        izq = a[:medio]
        dcha = a[medio:]
        dyc_izq = divideDyC2(izq) #recursion para la mitad izq
        dyc_dcha = divideDyC2(dcha) #recursion para mitad dcha
        if dyc_izq[1] < dyc_dcha[1]: #compara distancias dentro de las mitades
            minimo = dyc_izq
        else:
            minimo = dyc_dcha
        #combinar resultado comparando puntos en mitades diferentes                
        minimo = combinar(izq,dcha,minimo) 
                               
        
        return minimo # devuelve tupla (parMinimo, distanciaMinima)

            
def combinar(a,b,c): #recibe mitad izquierda(a), mitad derecha(b) y cota(c)
    medio = (a[-1][0] + b[0][0])/2 #tomo un valor medio de x (a partir del ultimo elemento de la izq y el primero de la dcha)para evaluar lo que sucede entre bloques
    liX = medio - c[1] #definimos limite inferior en eje x
    lsX = medio + c[1] # definimos limite superior en eje x
    entreBloques = False #partimos de la base de que no hay distancia minima entre bloque izq y dcho
    i = 0
    distEntre = [] #lista auxiliar que toma las distancias entre bloques que cumplen con los limites establecidos
    while i < len(a):
        liY = a[i][1] - c[1]#definimos limite inferior en el ejeY
        lsY = a[i][1] + c[1] #definimos limite superior en el ejeY
        if a[i][0]>= liX:
            # aca queremos chequear si hay un punto de b en los limites establecidos.
            puntosEnCota = [a[i]] #generamos una lista que guarde puntos que se encuentren dentro de los limites
            for elem in b:
                if elem[0] <= lsX and elem[1] >= liY and elem[1] <= lsY:
                    puntosEnCota.append(elem)
            if distanciaMinima(puntosEnCota) != False: #no queremos que apendee un False
                distEntre.append(distanciaMinima(puntosEnCota))
        i += 1
    if len(distEntre) > 0: #chqueamos el minimo de la lista distEntre
        ref = distEntre[0] 
        for n in range(len(distEntre)): 
            if distEntre[n][1] < ref[1]:
                ref = distEntre[n]
            if ref[1] < c[1]:
                entreBloques = ref
    if entreBloques != False and entreBloques[1] < c[1]:
        return entreBloques
    else:
        return c
                    
 

#Experimento: anAlisis de los tiempos de ejecucion
#La idea es comparar con una misma lista de tuplas aleatoria la distancia minima calculada con fuerza bruta (FB) y DivideConquer (DyC)
#Como resultado deberiamos ver que la distancia minima calculada es la misma independientemente del metodo y el algoritmo de ordenamiento
#Ademas, nos interesa ver los tiempos de ejecucion variando la cantidad de puntos y los limites

cantidadPuntos = 1000 #cantidad de puntos que quieren ser analizados. 

limites = 10000 #aqui van los limites de los ejes x e y. Arbitrariamente elegimos que van de -10000 a 10000.

tuplasAleatorias = [ ( random.randint(-limites,limites ), random.randint(-limites, limites) ) for k in range(cantidadPuntos) ]
inicio = time.time()
print('DistanciaMinimaFB', distanciaMinima(tuplasAleatorias))
final = time.time()
tiempo1 = float(final - inicio)
print('tiempoFB', tiempo1)
inicio = time.time()
print('DistanciaMinimaDyC ordenamiento UP',distanciaMinimaDyC(tuplasAleatorias,up))
final = time.time()
tiempo2 = float(final-inicio)
print('tiempo DyC ordenamiento UP', tiempo2)
inicio = time.time()
print('DistanciaMinimaDyC ordenamiento MERGE',distanciaMinimaDyC(tuplasAleatorias,merge))
final = time.time()
tiempo3 = float(final-inicio)
print('tiempo DyC ordenamiento MERGE', tiempo3)
inicio = time.time()
print('DistanciaMinimaDyC ordenamiento PYTHON',distanciaMinimaDyC(tuplasAleatorias,python))
final = time.time()
tiempo4 = float(final-inicio)
print('tiempo DyC ordenamiento PYTHON', tiempo4)